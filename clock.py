from apscheduler.schedulers.blocking import BlockingScheduler
import app
import datetime
import school_meal_parser
import pymongo
import os


sched = BlockingScheduler()


@sched.scheduled_job('cron', day_of_week='mon-fri', hour=8)
def scheduled_job():
    today = datetime.datetime.now()
    today_meal = school_meal_parser.get_result(today)

    if today_meal:
        def send_morning_message():
            user_dict = {}
            uri = os.environ['PROD_MONGODB']
            client = pymongo.MongoClient(uri)
            db = client.get_database()
            dblist = db['userlist']

            for i in dblist.find({"is_noti_agree": True}):
                user_dict[i['user_id']] = i['name']

            for user_id in user_dict:
                response_message = f'안녕하세요, {user_dict[user_id]}님? 좋은 아침이에요.\n오늘의 급식은\n{today_meal}\n입니다. 즐거운 하루 되세요!'
                app.send_message(user_id, response_message)

            client.close()

        send_morning_message()


sched.start()
