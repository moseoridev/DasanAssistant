import re

import requests
from bs4 import BeautifulSoup


def get_html(url):
    _html = ""
    resp = requests.get(url)
    if resp.status_code == 200:
        _html = resp.text
    return _html


def url_maker(date_time):
    """나이스 급식 URL을 만드는 함수

    class 'datetime.datetime'을 입력값으로, string url을 출력합니다.
    변수 url을 필요한 학교에 맞춰 수정해야 합니다.
    이 예제는 수원다산중학교의 경우이며 수정 방법은 https://github.com/agemor/school-api/blob/master/README.md를 참조해주세요."""

    url = "https://stu.goe.go.kr/sts_sci_md00_001.do?schulCode=J100006781&schulCrseScCode=3&schulKndScCode=03&schYm=" + date_time.strftime(
        "%Y%m")
    return url


def make_menu_list(time_inp):
    """선택된 달의 모든 급식을 리스트로 출력하는 함수"""
    html = get_html(url_maker(time_inp))
    souped_html = BeautifulSoup(html, "html.parser")
    raw_menu_list = souped_html.find("table", {"class": "tbl_type3 tbl_calendar"}).find_all("div")
    return raw_menu_list


def find_todays_meal(dt_inp):
    """입력된 날짜의 급식을 찾습니다."""
    for i in make_menu_list(dt_inp):
        try:
            if re.findall('\d+', str(i))[0] == str(dt_inp.day):
                return str(i)
        except:
            pass


def get_result(inp_dt):
    """최종 결과를 구합니다.
    급식 정보가 있는 날은 급식 정보를 string으로
    없는 날은 boolean False를 리턴합니다."""
    raw = find_todays_meal(inp_dt)

    def rm_tango(i):
        tango_list = ('.', '\'', '`', '<div>', '</div>', '[중식]', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9')
        for c in tango_list:
            i = i.replace(c, '')
        return i

    raw = rm_tango(raw).replace('<br/>', '\n')
    if not raw == '':
        return raw[2:]
    elif raw == '':
        return False
