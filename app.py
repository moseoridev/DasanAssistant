import datetime
import json
import os

import pymongo
import requests
from flask import Flask, request

import school_meal_parser

app = Flask(__name__)
ACCESS_TOKEN = os.environ['ACCESS_TOKEN']
VERIFY_TOKEN = os.environ['VERIFY_TOKEN']


@app.route("/webhook", methods=['GET', 'POST'])
def receive_message():
    if request.method == 'GET':
        token_sent = request.args.get("hub.verify_token")
        return verify_fb_token(token_sent)
    else:
        output = request.get_json()
#        print(str(output))
        for event in output['entry']:
            try:
                messaging = event['messaging']
                for message in messaging:
                    if message.get('message'):
                        recipient_id = message['sender']['id']
                        if message['message'].get('text'):
                            if message['message'].get('text') == "버전":
                                response_sent_text = ('다산 어시스턴트 1.0.3\nMade by Moseoridev')
                            elif message['message'].get('text') == "개발자":
                                response_sent_text = "개발자: 30109 신준서\n특별 테스터: 30106 박서원"
                            elif message['message'].get('text') == "명령어":
                                response_sent_text = ''
                                send_message(recipient_id, '아래 명령어 중 하나를 선택해보세요.')
                            elif "급식" in message['message'].get('text'):
                                try:
                                    if message['message']['nlp']['entities']['datetime']:
                                        for a in message['message']['nlp']['entities']['datetime']:
                                            value = a['value']
                                            value = datetime.datetime.strptime(value[0:value.index('T')], '%Y-%m-%d')
                                        if school_meal_parser.get_result(value):
                                            response_sent_text = school_meal_parser.get_result(value)
                                        else:
                                            response_sent_text = "선택하신 날짜에는 급식을 실시하지 않거나, 아직 급식 정보가 제공되지 않습니다. 알맞은 날짜" \
                                                                 "를 입력해주세요."
                                except:
                                    response_sent_text = "선택하신 날짜에는 급식을 실시하지 않거나, 아직 급식 정보가 제공되지 않습니다. 알맞은 날짜를 입력해주세요."
                            elif message['message'].get('text') == "내 ID":
                                response_sent_text = message['sender']['id']
                            elif message['message'].get('text') == "수신 동의":
                                noti_agree(recipient_id)
                                response_sent_text = ''
                            elif message['message'].get('text') == "수신 거부":
                                noti_disagree(recipient_id)
                                response_sent_text = ''
                            elif message['message'].get('text') == "현재 시각":
                                response_sent_text = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
                            elif message['message'].get('text'):
                                response_sent_text = "당신의 메시지는 " + message['message']['text'] + " 입니다."
                            send_message(recipient_id, response_sent_text)
                    elif message.get('postback'):
                        print(message.get('postback'))
                        recipient_id = message['sender']['id']
                        if message['postback'].get('payload'):
                            if message['postback']['payload'] == 'get_started':
                                get_started(message)
                            elif message['postback']['payload'] == 'noti_agree':
                                pass
                            elif message['postback']['payload'] == 'noti_disagree':
                                pass
                            else:
                                send_message(recipient_id, "알 수 없는 payload 수신됨: " + message['postback']['payload'])
                    else:
                        print("Webhook received unknown messagingEvent: " + str(message))
            except KeyError as e:
                print(f"KeyError가 발생했습니다. 에러 메시지:{e}")
    return "Message Processed"


def verify_fb_token(token_sent):
    if request.args.get('hub.mode') == 'subscribe' and token_sent == VERIFY_TOKEN:
        print("Webhook 검증 성공")
        return request.args.get("hub.challenge")
    print('Webhook 검증 실패')
    return '바르지 않은 검증 토큰'


def send_raw(payload):
    request_endpoint = 'https://graph.facebook.com/v2.12/me/messages'

    response = requests.post(
        request_endpoint,
        params={'access_token': ACCESS_TOKEN},
        json=payload
    )

    result = response.json()

    return result


def send_message(recipient_id, text):
    payload = {"recipient": {"id": recipient_id}, "message": {
        "text": text,
        "quick_replies": [
            {
                "content_type": "text",
                "title": "내일 급식",
                "payload": "qr_mealex",
                "image_url": "https://png.icons8.com/ios/50/000000/meal.png"
            },
            {
                "content_type": "text",
                "title": "버전",
                "payload": "qr_version",
                "image_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Pictogram_voting_question.svg/220px-Pictogram_voting_question.svg.png"
            },
            {
                "content_type": "text",
                "title": "개발자",
                "payload": "qr_devs",
                "image_url": "http://icons.iconarchive.com/icons/graphicloads/100-flat-2/256/people-icon.png"
            },
            {
                "content_type": "text",
                "title": "명령어",
                "payload": "qr_commands",
                "image_url": "https://www.omgubuntu.co.uk/wp-content/uploads/2016/09/terminal-icon.png"
            }
        ]}}
    send_raw(payload)


def get_started(message):
    user_id = message['sender']['id']
    user_name = get_username(user_id)

    def check_user_in_db():
        result = ''
        uri = os.environ['PROD_MONGODB']
        client = pymongo.MongoClient(uri)
        db = client.get_database()
        dblist = db['userlist']

        for i in dblist.find({'user_id': user_id}):
            result += str(i)

        if result == '':
            query = {
                'user_id': user_id,
                'name': user_name,
                'is_noti_agree': True,
                'first_date': datetime.datetime.now().strftime('%Y-%m-%d')
            }
            dblist.insert(query)

        client.close()

    check_user_in_db()

    payload = {"recipient": {"id": message['sender']['id']}, "message": {
        "text": f"안녕하세요, {user_name}님! 당신의 다산중 생활을 도와드리는 다산 어시스턴트입니다.\n매일 아침 8시에 오늘의 급식 정보를 전송해 드리고자 합니다.\n알림 수신에 동의하십니까?",
        "quick_replies": [
            {
                "content_type": "text",
                "title": "수신 동의",
                "payload": "noti_agree",
                "image_url": "https://cdn0.iconfinder.com/data/icons/toolbar-signs-1/512/accept_add_agree_apply_approve_approved_certificate_check_checkbox_checkmark_choice_choose_confirm_correct_done_good_green_invoice_mark_ok_order_process_ready_right_select_sign_start_success_test_thum-512.png"
            },
            {
                "content_type": "text",
                "title": "수신 거부",
                "payload": "noti_disagree",
                "image_url": "https://cdn4.iconfinder.com/data/icons/communication-142/32/No-512.png"
            }
        ]}}
    send_raw(payload)


def noti_agree(user_id):
    uri = os.environ['PROD_MONGODB']
    client = pymongo.MongoClient(uri)
    db = client.get_database()
    dblist = db['userlist']

    dblist.update({'user_id': user_id}, {'$set': {'is_noti_agree': True}})

    send_message(user_id, '급식 알림 수신을 동의하셨습니다. 변경하시고 싶으시다면 수신 거부 라고 입력해주세요.')


def noti_disagree(user_id):
    uri = os.environ['PROD_MONGODB']
    client = pymongo.MongoClient(uri)
    db = client.get_database()
    dblist = db['userlist']

    dblist.update({'user_id': user_id}, {'$set': {'is_noti_agree': False}})

    send_message(user_id, '급식 알림 수신을 거부하셨습니다. 변경하시고 싶으시다면 수신 동의 라고 입력해주세요.')


def get_username(user_id):
    token = ACCESS_TOKEN

    url = f'https://graph.facebook.com/v2.6/{user_id}?fields=first_name,last_name,profile_pic&access_token={token}'

    r = requests.get(url)

    parsed = json.loads(r.content)

    return parsed['last_name'] + parsed['first_name']


if __name__ == "__main__":
    app.run(debug=True)
